package variables;

/**
 * Created by erik.schults on 25.10.2016.
 */
public class primitives {
    public static void main(String[] args) {
        int saas = 122_213_3122;
        System.out.println(saas); //prints 1222133122
    }

    /**
     * objects store their invidual states in "non-staic fields", declared without the 'static' keyword
     * they are also called instance variables, value is unique to each instance of a class
     *
     * a class variable is any field declared with the 'static' modifier; only one per class
     * aka a bicycle only has 6 gears; int numGears = 6
     * 'final int' would mean that the variable will never change
     *
     * local variables are variables in which methods store their temporary state; int local = 1
     * they are only visible to the method
     *
     * parameters are 'variables' which a method is called upon; getCoffee(params)
     *
     * cosntant value names such as static int final NUMBER = 5; uppercase
     * */

    // byte; min value -128, max 127; integer value; use to save memory
    byte newByte = 10;

    // short; min value -32,768, max 32767; integer value; use to save memory
    short newShort = 9992;

    // int; min value -2^31, max 2^31-1; signed integer; unsigned means that values range from 0 to 2^31-1
    int newInt = 992929932;
    int hexInt = 0x1a;

    // long; twice as big as int;
    long newLong = 1923912312412412490L;

    // float; decimal number, 32-bit; should not be used for exact values(currency), use java.math.BigDecimal instead
    float newFloat = 2.123124f; // f is required, otherwise it is a double

    // double; decimal number, 64-bit; should not be used for exact values(currency), use java.math.BigDecimal instead
    double newDouble = 2.123124;
    double scienceD = 2.123e3;

    // boolean; true & falsel ASJDKLASDLASJD
    boolean notTrue = true;

    // char; singe 16-bit unicode character; min value '\u0000', max '\uffff', single quotes ' '
    char letterF = 'F';

    // String, not a primitive type, DOULBE QUOTES " "
    String gS = "this is a string";
}
