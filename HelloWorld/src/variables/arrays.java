package variables;

/**
 * Created by erik.schults on 25.10.2016.
 */
public class arrays {
    public static void main(String[] args) {
        /*
         * array declaration
         * type[] name;
         * */
        int[] anArray; // declare an array of integers
        anArray = new int[10]; // allocates memovry for 10 integerse

        // also this is possible
        boolean[] Bools = {true, false, true, false};

        // multidimensional arrays
        String[][] multiArr = {
                {"lol", "nice"},
                {"what", "ever"}
        };

        anArray[0] = 1231;
        anArray[1] = 3231;
        // wont work anArray[10] = 211;
        anArray[9] = 222;

        System.out.println(multiArr[0][0] + " " + multiArr[1][0]);
        System.out.println(multiArr.length);

        // arraycopy method
        char[] from1 = {'a', 'b', 'c', 'd', 'e', 'f'};
        char[] to1 = new char[6];

        System.arraycopy(from1, 2, to1, 2, 4);
        System.out.println(new String(to1));

        // java.util.Arrays.copyOfRange doesnt require you to declare a variable beforehand
        char[] from2 = {'d', 'e', 'c', 'a', 'f', 'f', 'e', 'i', 'n', 'a', 't', 'e', 'd'};
        char[] from3 = {'d', 'e', 'c', 'a', 'f', 'f', 'e', 'i', 'n', 'a', 't', 'e', 'd'};

        char[] to2 = java.util.Arrays.copyOfRange(from2, 2, 9);
        System.out.println(new String(to2));

        System.out.println(java.util.Arrays.binarySearch(from2, 'e'));

        System.out.println(java.util.Arrays.equals(from2, from3));

        System.out.println(java.util.Arrays.toString(anArray));
        java.util.Arrays.fill(anArray, 3);
        System.out.println(java.util.Arrays.toString(anArray));
    }

}
