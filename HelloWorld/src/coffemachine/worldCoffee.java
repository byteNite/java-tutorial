package coffemachine;

/**
 * Created by erik.schults on 25.10.2016.
 */
class worldCoffee implements coffeeMachine {
    public static void main(String[] args) {
        worldCoffee worldCoffeeMachine = new worldCoffee();
        worldCoffeeMachine.start();
    }

    int state = 0;
    int sugar = 0;
    int stiffness = 1;
    int milk = 0;

    public void toggleState(int newState) {
        // off = 0
        // standby = 1
        // on = 2
        state = newState;
    };

    public void start() {
        System.out.println("Pouring coffee");
    };
    public void end() {
        System.out.println("Stopped pouring coffee");
    };
    public void addSugar(int incr) {
        sugar += incr;
        System.out.println(this.sugar);
    };
    public void decSugar(int decr) {
        sugar += decr;
    };
    public void addStiffness(int incr) {
        stiffness += incr;
    };
    public void decStiffness(int decr) {
        stiffness += decr;
    };
    public void addMilk(int incr) {
        milk += incr;
    };
    public void decMilk(int decr) {
        milk += decr;
    };

}
