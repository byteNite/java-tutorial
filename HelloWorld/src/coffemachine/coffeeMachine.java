package coffemachine;

/**
 * Created by erik.schults on 25.10.2016.
 */

interface coffeeMachine {
    int state = 0;
    int sugar = 0;
    int stiffness = 1;
    int milk = 0;

    void toggleState(int newValue);
    void start();
    void end();
    void addSugar(int incr);
    void decSugar(int decr);
    void addStiffness(int incr);
    void decStiffness(int decr);
    void addMilk(int incr);
    void decMilk(int decr);
}
